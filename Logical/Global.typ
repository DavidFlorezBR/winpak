
TYPE
	HmiButtonOutputs_typ : 	STRUCT 
		Style : STRING[80];
		imagePath : STRING[80];
	END_STRUCT;
	HmiCtrlInput_typ : 	STRUCT 
		bHMI_TestButton : BOOL;
		Outputs : USINT;
	END_STRUCT;
	HmiCtrlInternal_typ : 	STRUCT 
		PLSChannels : ARRAY[0..8]OF STRING[80] := ['Registration','Seals','Filler','Idle Mode','Pull Wheels','Perforator','Plungers','Knife Blade','Knife Clamp',''];
		Outputs : USINT;
		PLSOff : ARRAY[0..8]OF REAL;
		PLSOn : ARRAY[0..8]OF REAL;
	END_STRUCT;
	HmiCtrlOutput_typ : 	STRUCT 
		PumpStatusTextOutput : HmiCtrlTextOutputs_typ;
		AlarmStatusFlyoutButton : HmiButtonOutputs_typ;
		RecipeListDataProvider : ARRAY[0..29]OF STRING[255];
		OpcUaLine1 : STRING[20];
	END_STRUCT;
	HmiCtrlTextOutputs_typ : 	STRUCT 
		Style : STRING[80];
	END_STRUCT;
	HmiCtrl_typ : 	STRUCT 
		Inputs : HmiCtrlInput_typ;
		Outputs : HmiCtrlOutput_typ;
		Internal : HmiCtrlInternal_typ;
	END_STRUCT;
	MainCtrlStatus_typ : 	STRUCT 
		AlarmActive : BOOL;
		UserDeviceConnected : BOOL;
		AlarmSystemReady : BOOL;
	END_STRUCT;
	MainCtrl_typ : 	STRUCT 
		Par : USINT;
		Status : MainCtrlStatus_typ;
		Cmd : USINT;
	END_STRUCT;
END_TYPE
