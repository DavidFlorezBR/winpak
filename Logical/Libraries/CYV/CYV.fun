
FUNCTION_BLOCK OpcUaCommunication
	VAR_INPUT
		MpLink : MpComIdentType; (* *) (* *) (*#PAR#*)
		Enable : BOOL; (* *) (* *) (*#PAR#*)
		Parameters : REFERENCE TO OpcUaCommunicationParType; (*Parameters OF the FUNCTION block*) (* *) (*#PAR#*)
		pVariables : REFERENCE TO VariableType; (*Pointer TO the ARRAY OF variables that should be exchanged*) (* *) (*#CMD#*)
		NumberOfVariables : UINT; (*number OF variables TO be exchanged*) (* *) (*#CMD#*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (* *) (* *) (*#PAR#*)
		Error : BOOL; (* *) (* *) (*#PAR#*)
		StatusID : DINT; (* *) (* *) (*#PAR#*)
		Info : OpcUaCommunicationInfoType; (* *) (* *) (*#CMD#*)
	END_VAR
	VAR
		ident : UDINT;
		AlarmXConfiguration : MpAlarmXAlarmConfigType;
	END_VAR
END_FUNCTION_BLOCK
