
TYPE
	OpcUaCommunicationParType : 	STRUCT 
		ChannelName : STRING[80];
		PlcName : STRING[80];
		ServerIpAddress : STRING[15];
		ServerPortNumber : UINT;
		ServerTimeout : TIME := T#2s;
	END_STRUCT;
	VariableType : 	STRUCT 
		VariableName : STRING[CYV_MAX_LENGTH_VARIABLE_INDEX]; (*The name of the variable*)
		ServerName : STRING[CYV_MAX_LENGTH_VARIABLE_INDEX];
		NamespaceUri : STRING[255];
		NamespaceIndex : USINT := 2;
		RefreshTime : TIME; (*Defined refresh time*)
		ReadOnly : BOOL := FALSE;
	END_STRUCT;
	OpcUaCommunicationInfoType : 	STRUCT 
		NumberOfConnectedVariables : UDINT;
		ConnectionHdl : DWORD;
		SubscriptionHdl : DWORD;
		Diag : OpcUaCommunicationDiagType; (*Diagnostic structure for the status ID*)
	END_STRUCT;
	OpcUaCommunicationDiagType : 	STRUCT 
		StatusID : OpcUaCommunicationStatusIDType; (*StatusID diagnostics structure*)
	END_STRUCT;
	OpcUaCommunicationStatusIDType : 	STRUCT 
		ID : OpcUaCommunicationErrorEnum; (*Error code for component*)
		Severity : MpComSeveritiesEnum; (*Severity of the error*)
		Code : UINT; (*Code for the status ID. This error number can be used to search for additional information in the help system.*)
	END_STRUCT;
END_TYPE
