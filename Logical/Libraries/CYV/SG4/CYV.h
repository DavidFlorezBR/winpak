/* Automation Studio generated header file */
/* Do not edit ! */
/* CYV 1.04.0 */

#ifndef _CYV_
#define _CYV_
#ifdef __cplusplus
extern "C" 
{
#endif
#ifndef _CYV_VERSION
#define _CYV_VERSION 1.04.0
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
#ifdef _SG3
		#include "AsOpcUac.h"
		#include "AsICMP.h"
		#include "sys_lib.h"
		#include "ArEventLog.h"
		#include "MpAlarmX.h"
		#include "MpBase.h"
#endif
#ifdef _SG4
		#include "AsOpcUac.h"
		#include "AsICMP.h"
		#include "sys_lib.h"
		#include "ArEventLog.h"
		#include "MpAlarmX.h"
		#include "MpBase.h"
#endif
#ifdef _SGC
		#include "AsOpcUac.h"
		#include "AsICMP.h"
		#include "sys_lib.h"
		#include "ArEventLog.h"
		#include "MpAlarmX.h"
		#include "MpBase.h"
#endif


/* Constants */
#ifdef _REPLACE_CONST
 #define CYV_MAX_LENGTH_VARIABLE_INDEX 256U
#else
 _GLOBAL_CONST unsigned short CYV_MAX_LENGTH_VARIABLE_INDEX;
#endif




/* Datatypes and datatypes of function blocks */
typedef enum OpcUaCommunicationErrorEnum
{	OpcUa_NO_ERROR = 0,
	OpcUa_UNKNOWN_NAMESPACE_URI = -535887860,
	OpcUa_OUT_OF_MEMORY = -535887861,
	OpcUa_UNKNOWN_VARIABLE = -535887862,
	OpcUa_UNKNOWN_SERVER_STATE = -535887863,
	OpcUa_FAILED_TO_SUBSCRIBE = -535887864,
	OpcUa_UNSUPPORTED_DATATYPE = -535887865,
	OpcUa_ERR_PAR_NULL = -535887866,
	OpcUa_FAILED_TO_READ_VARIABLE = -535887867,
	OpcUa_FAILED_TO_WRITE_VARIABLE = -535887868,
	OpcUa_FAILED_TO_CONNECT_VARIABLE = -535887869,
	OpcUa_LOSS_OF_COMMUNICATION = -535887870,
	OpcUa_CONNECTION_ERROR = -535887871
} OpcUaCommunicationErrorEnum;

typedef struct OpcUaCommunicationParType
{	plcstring ChannelName[81];
	plcstring PlcName[81];
	plcstring ServerIpAddress[16];
	unsigned short ServerPortNumber;
	plctime ServerTimeout;
} OpcUaCommunicationParType;

typedef struct VariableType
{	plcstring VariableName[257];
	plcstring ServerName[257];
	plcstring NamespaceUri[256];
	unsigned char NamespaceIndex;
	plctime RefreshTime;
	plcbit ReadOnly;
} VariableType;

typedef struct OpcUaCommunicationStatusIDType
{	enum OpcUaCommunicationErrorEnum ID;
	MpComSeveritiesEnum Severity;
	unsigned short Code;
} OpcUaCommunicationStatusIDType;

typedef struct OpcUaCommunicationDiagType
{	struct OpcUaCommunicationStatusIDType StatusID;
} OpcUaCommunicationDiagType;

typedef struct OpcUaCommunicationInfoType
{	unsigned long NumberOfConnectedVariables;
	plcdword ConnectionHdl;
	plcdword SubscriptionHdl;
	struct OpcUaCommunicationDiagType Diag;
} OpcUaCommunicationInfoType;

typedef struct OpcUaCommunication
{
	/* VAR_INPUT (analog) */
	struct MpComIdentType MpLink;
	struct OpcUaCommunicationParType* Parameters;
	struct VariableType* pVariables;
	unsigned short NumberOfVariables;
	/* VAR_OUTPUT (analog) */
	signed long StatusID;
	struct OpcUaCommunicationInfoType Info;
	/* VAR (analog) */
	unsigned long ident;
	struct MpAlarmXAlarmConfigType AlarmXConfiguration;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Error;
} OpcUaCommunication_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void OpcUaCommunication(struct OpcUaCommunication* inst);


__asm__(".section \".plc\"");

/* Additional IEC dependencies */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/AsOpcUac/AsOpcUac.var\\\" scope \\\"global\\\"\\n\"");

__asm__(".previous");

#ifdef __cplusplus
};
#endif
#endif /* _CYV_ */

