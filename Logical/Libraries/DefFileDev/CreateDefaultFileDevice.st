
FUNCTION_BLOCK CreateDefaultFileDevice
	//If simulation is active then link Device names to local folders
	IF DiagCpuIsSimulated() OR DiagCpuIsARsim() THEN
		IF NOT Connected THEN
			Path := '/DEVICE=C:/Temp/Sim/User';
			DevLink_0.enable := 1;
			DevLink_0.pDevice := ADR(DeviceName);
			DevLink_0.pParam := ADR(Path);
			DevLink_0();
			
			IF (DevLink_0.status = ERR_OK) THEN
				Connected := 1;
				DevLink_0.enable := 0; 
			END_IF;
		END_IF; 
	ELSE
		Path :=  '/DEVICE=F:/';
		IF NOT Connected THEN
			DevLink_0.enable := 1;
			DevLink_0.pDevice := ADR(DeviceName);
			DevLink_0.pParam := ADR(Path);
			DevLink_0();
			
			IF (DevLink_0.status = ERR_OK) THEN
				Connected := 1;
				DevLink_0.enable := 0; 
			END_IF;
		END_IF;
	END_IF;
END_FUNCTION_BLOCK
