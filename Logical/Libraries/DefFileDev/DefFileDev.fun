
FUNCTION_BLOCK CreateDefaultFileDevice
	VAR_OUTPUT
		Connected : BOOL;
	END_VAR
	VAR
		DevLink_0 : DevLink;
	END_VAR
	VAR CONSTANT
		DeviceName : STRING[80] := 'USER';
	END_VAR
	VAR
		Path : STRING[80];
	END_VAR
END_FUNCTION_BLOCK
