
PROGRAM _INIT
	strHMI_CurrentPage;
END_PROGRAM

PROGRAM _CYCLIC
	DTGetTime_0(enable :=1 );
	//gHmiCtrl.Outputs.dtHMI_CurrentDateTime := DTGetTime_0.DT1;  
	
	IF gMainCtrl.Status.AlarmActive THEN
	  gHmiCtrl.Outputs.AlarmStatusFlyoutButton.imagePath := ALARM_RED_IMAGE;
	ELSE
		gHmiCtrl.Outputs.AlarmStatusFlyoutButton.imagePath := ALARM_BLACK_IMAGE;
	END_IF;

	//Example of switching style names based off a variable
	IF OpcUaActive THEN
		gHmiCtrl.Outputs.OpcUaLine1 := 'Green_Line';
	ELSE
		gHmiCtrl.Outputs.OpcUaLine1 := RED_LINE;
	END_IF;
END_PROGRAM


