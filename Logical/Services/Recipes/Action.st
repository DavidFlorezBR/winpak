
ACTION Format_DataProvider:
	brsmemset(ADR(gHmiCtrl.Outputs.RecipeListDataProvider),0,SIZEOF(gHmiCtrl.Outputs.RecipeListDataProvider));
	FOR idx := 0 TO 19 DO
		brsstrcpy(ADR(gHmiCtrl.Outputs.RecipeListDataProvider[idx]), ADR('{"value":"'));
		brsstrcat(ADR(gHmiCtrl.Outputs.RecipeListDataProvider[idx]), ADR(UiConnect.Recipe.List.Names[idx]));
		brsstrcat(ADR(gHmiCtrl.Outputs.RecipeListDataProvider[idx]), ADR('","text":"'));
		brsstrcat(ADR(gHmiCtrl.Outputs.RecipeListDataProvider[idx]), ADR(UiConnect.Recipe.List.Names[idx]));
		brsstrcat(ADR(gHmiCtrl.Outputs.RecipeListDataProvider[idx]), ADR('"}'));
	END_FOR;
END_ACTION
