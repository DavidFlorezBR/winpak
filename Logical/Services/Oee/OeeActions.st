
ACTION OeeHelper: 
	MpOeeCore_0.PieceCounter := MpOeeCore_0.PieceCounter + 1;
	
   MpOeeListUI_0.UISetup.ScrollWindow := 1;
    MpOeeListUI_0.UISetup.OutputListSize := 5;
    
    IF ListFromEnable <> MpOeeListUIConnect.Filter.Current.From.Enable THEN
        MpOeeListUIConnect.Filter.Dialog.From.Enable := ListFromEnable;
        MpOeeListUIConnect.Filter.ShowDialog := 1;
        MpOeeListUIConnect.Filter.Dialog.Confirm := 1;
    END_IF
    IF ListFrom <> MpOeeListUIConnect.Filter.Current.From.DateTime THEN
        DT_TO_DTStructure(ListFrom, ADR(ListFromStruct));
        MpOeeListUIConnect.Filter.Dialog.From.Year := ListFromStruct.year;
        MpOeeListUIConnect.Filter.Dialog.From.Month := ListFromStruct.month;
        MpOeeListUIConnect.Filter.Dialog.From.Day := ListFromStruct.day;
        MpOeeListUIConnect.Filter.Dialog.From.Hour := ListFromStruct.hour;
        MpOeeListUIConnect.Filter.Dialog.From.Minute := ListFromStruct.minute;
        MpOeeListUIConnect.Filter.ShowDialog := 1;
        MpOeeListUIConnect.Filter.Dialog.Confirm := 1;
    END_IF
    IF ListToEnable <> MpOeeListUIConnect.Filter.Current.Until.Enable THEN
        MpOeeListUIConnect.Filter.Dialog.Until.Enable := ListToEnable;
        MpOeeListUIConnect.Filter.ShowDialog := 1;
        MpOeeListUIConnect.Filter.Dialog.Confirm := 1;
    END_IF
    IF ListTo <> MpOeeListUIConnect.Filter.Current.Until.DateTime THEN
        DT_TO_DTStructure(ListTo, ADR(ListToStruct));
        MpOeeListUIConnect.Filter.Dialog.Until.Year := ListToStruct.year;
        MpOeeListUIConnect.Filter.Dialog.Until.Month := ListToStruct.month;
        MpOeeListUIConnect.Filter.Dialog.Until.Day := ListToStruct.day;
        MpOeeListUIConnect.Filter.Dialog.Until.Hour := ListToStruct.hour;
        MpOeeListUIConnect.Filter.Dialog.Until.Minute := ListToStruct.minute;
        MpOeeListUIConnect.Filter.ShowDialog := 1;
        MpOeeListUIConnect.Filter.Dialog.Confirm := 1;
    END_IF
    
    
    MpOeeTimelineUI_0.UISetup.ScrollWindow := 1;
    MpOeeTimelineUI_0.UISetup.TimelineListSize := 5;
    
    IF TimelineFromEnable <> MpOeeTimelineUIConnect.Filter.Current.From.Enable THEN
        MpOeeTimelineUIConnect.Filter.Dialog.From.Enable := TimelineFromEnable;
        MpOeeTimelineUIConnect.Filter.ShowDialog := 1;
        MpOeeTimelineUIConnect.Filter.Dialog.Confirm := 1;
    END_IF
    IF TimelineFrom <> MpOeeTimelineUIConnect.Filter.Current.From.DateTime THEN
        DT_TO_DTStructure(TimelineFrom, ADR(TimelineFromStruct));
        MpOeeTimelineUIConnect.Filter.Dialog.From.Year := TimelineFromStruct.year;
        MpOeeTimelineUIConnect.Filter.Dialog.From.Month := TimelineFromStruct.month;
        MpOeeTimelineUIConnect.Filter.Dialog.From.Day := TimelineFromStruct.day;
        MpOeeTimelineUIConnect.Filter.Dialog.From.Hour := TimelineFromStruct.hour;
        MpOeeTimelineUIConnect.Filter.Dialog.From.Minute := TimelineFromStruct.minute;
        MpOeeTimelineUIConnect.Filter.ShowDialog := 1;
        MpOeeTimelineUIConnect.Filter.Dialog.Confirm := 1;
    END_IF
    IF TimelineToEnable <> MpOeeTimelineUIConnect.Filter.Current.Until.Enable THEN
        MpOeeTimelineUIConnect.Filter.Dialog.Until.Enable := TimelineToEnable;
        MpOeeTimelineUIConnect.Filter.ShowDialog := 1;
        MpOeeTimelineUIConnect.Filter.Dialog.Confirm := 1;
    END_IF
    IF TimelineTo <> MpOeeTimelineUIConnect.Filter.Current.Until.DateTime THEN
        DT_TO_DTStructure(TimelineTo, ADR(TimelineToStruct));
        MpOeeTimelineUIConnect.Filter.Dialog.Until.Year := TimelineToStruct.year;
        MpOeeTimelineUIConnect.Filter.Dialog.Until.Month := TimelineToStruct.month;
        MpOeeTimelineUIConnect.Filter.Dialog.Until.Day := TimelineToStruct.day;
        MpOeeTimelineUIConnect.Filter.Dialog.Until.Hour := TimelineToStruct.hour;
        MpOeeTimelineUIConnect.Filter.Dialog.Until.Minute := TimelineToStruct.minute;
        MpOeeTimelineUIConnect.Filter.ShowDialog := 1;
        MpOeeTimelineUIConnect.Filter.Dialog.Confirm := 1;
    END_IF
    
    FOR i := 0 TO 4 DO
        TimeString := UDINT_TO_STRING(MpOeeTimelineUIConnect.Output.Display[i].Duration.Hours);
        IF MpOeeTimelineUIConnect.Output.Display[i].Duration.Hours < 10 THEN
            TmpString := '0';
            brsstrcat(ADR(TmpString), ADR(TimeString));
            brsstrcat(ADR(TmpString), ADR(':'));
        ELSE
            TmpString := TimeString;
            brsstrcat(ADR(TmpString), ADR(':'));
        END_IF
        TimeString := UDINT_TO_STRING(MpOeeTimelineUIConnect.Output.Display[i].Duration.Minutes);
        IF MpOeeTimelineUIConnect.Output.Display[i].Duration.Minutes < 10 THEN
            brsstrcat(ADR(TmpString), ADR('0'));
            brsstrcat(ADR(TmpString), ADR(TimeString));
            brsstrcat(ADR(TmpString), ADR(':'));
        ELSE
            brsstrcat(ADR(TmpString), ADR(TimeString));
            brsstrcat(ADR(TmpString), ADR(':'));
        END_IF
        TimeString := UDINT_TO_STRING(MpOeeTimelineUIConnect.Output.Display[i].Duration.Seconds);
        IF MpOeeTimelineUIConnect.Output.Display[i].Duration.Seconds < 10 THEN
            brsstrcat(ADR(TmpString), ADR('0'));
            brsstrcat(ADR(TmpString), ADR(TimeString));
        ELSE
            brsstrcat(ADR(TmpString), ADR(TimeString));
        END_IF
        brsstrcpy(ADR(TimelineString[i]), ADR(TmpString));
        IF MpOeeTimelineUIConnect.Output.Display[i].ProductionState = mpOEE_STATE_UPTIME THEN
            TimelineStyle[i] := 'Progress_Green';
        ELSIF MpOeeTimelineUIConnect.Output.Display[i].ProductionState = mpOEE_STATE_SCHDL_DOWNTIME THEN
            TimelineStyle[i] := 'Progress_Yellow';
        ELSIF MpOeeTimelineUIConnect.Output.Display[i].ProductionState = mpOEE_STATE_UNSCHDL_DOWNTIME THEN
            TimelineStyle[i] := 'Progress_Red';
        ELSE
            TimelineStyle[i] := 'Progress_Gray';
        END_IF
    END_FOR
	

END_ACTION




ACTION ExitTask: 

	MpOeeCore_0(Enable := FALSE);

END_ACTION