
TYPE
	stHmiCtrl_typ : 	STRUCT  (*Main HMI structure*)
		stHMI_CSF1 : stHMIHeater_typ;
		stHMI_SSF1A : stHMIHeater_typ;
		bHMI_MachineStopped : BOOL;
		bHMI_Blinker_Comm : BOOL;
		bHMI_GuardCircuitAlarm : BOOL;
		bHMI_MachineMode_Run : BOOL;
		bHMI_Estop : BOOL;
		bHMI_MachineMode_ServoJog : BOOL;
		bHMI_AirPressureAlarm : BOOL;
		sHMI_PumpServoStatus : STRING[80];
		vMastMain_Pos : REAL;
		rHMI_AirPressureSensor : REAL;
		rHMI_Pump_Pos : REAL;
		bHMI_MachineMode_Jog : BOOL;
		bHMI_CIP_Auto_Mode : BOOL;
		bHMI_SS_Heaters_On_Cmd : BOOL;
		bHMI_CS_Heaters_On_Cmd : BOOL;
		rHMI_CIPAirPressureSensor : REAL;
		bHMI_TestButton : BOOL;
		bHMI_IdleMode : BOOL;
		bHMI_MachineMode_CIP : BOOL;
		bHMI_NoCupSensor_On : BOOL;
		bHMI_DenesterOn : BOOL;
		bTuckerBarOn : BOOL;
		iAnalog_Prod_Level : USINT;
		bHMI_FimlDriveOn : BOOL;
		stRecipeSettings : stHmiRecipeSettings_typ;
	END_STRUCT;
	stHMIHeater_typ : 	STRUCT  (*Heater control structure*)
		rActualTemp : REAL;
		bManualMode : BOOL;
		bAutoTune : BOOL;
	END_STRUCT;
	stHmiRecipeSettings_typ : 	STRUCT  (*Recipe structure*)
		RecipeValue : USINT;
	END_STRUCT;
END_TYPE
