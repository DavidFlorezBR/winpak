﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio FileVersion="4.9"?>
<Program SubType="IEC" xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File Description="Initialization code">Init.st</File>
    <File Description="Cyclic code">Cyclic.st</File>
    <File Description="Local data types" Private="true">CYV_Types.typ</File>
    <File Description="Local variables" Private="true">CYV_Vars.var</File>
    <File Description="User Defined Variables" Private="true">Variables.var</File>
  </Files>
</Program>