PROGRAM _CYCLIC
    
	//If there is no IP address then set to a default value
	IF brsstrlen(ADR(strMLC_IP_ADDRESS)) = 0 THEN
		strMLC_IP_ADDRESS := '129.1.1.4';
	END_IF;

	OpcUaParameters.ServerIpAddress := strMLC_IP_ADDRESS;
	OpcUaCommunication_0.Enable := gMainCtrl.Status.AlarmSystemReady AND EnableOpcUaComm;
	OpcUaCommunication_0.MpLink := gAlarmXCoreLink;
	OpcUaCommunication_0.Parameters := ADR(OpcUaParameters);
	OpcUaCommunication_0.pVariables := ADR(Variables);
	OpcUaCommunication_0.NumberOfVariables := SIZEOF(Variables) / SIZEOF(Variables[0]);
	OpcUaCommunication_0();

END_PROGRAM
